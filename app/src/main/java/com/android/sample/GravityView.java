/**
 * GravityView.java
 * Copyright (c) 2011 daoki2
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.android.sample;

import android.content.Context;
import android.renderscript.RSSurfaceView;
import android.renderscript.RenderScriptGL;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class GravityView extends RSSurfaceView {

    public GravityView(Context context) {
        super(context);
    }

    private RenderScriptGL mRS;
    private GravityRS mRender;

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        super.surfaceChanged(holder, format, w, h);
        if (mRS == null) {
            RenderScriptGL.SurfaceConfig sc = new RenderScriptGL.SurfaceConfig();
            mRS = createRenderScriptGL(sc);
            mRS.setSurface(holder, w, h);

            mRender = new GravityRS();
            mRender.init(mRS, getResources(), w, h);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mRS != null) {
            mRS = null;
            destroyRenderScriptGL();
        }

        super.onDetachedFromWindow();
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev)
    {
//        mRender.newTouchPosition(ev.getX(0), ev.getY(0), ev.getPressure(0), ev.getPointerId(0));

        /*
        Log.e("onTouch", "point count = " + ev.getPointerCount());
        Log.e("onTouch", "action = " + getMotionActionString(ev.getActionMasked()));
        Log.e("onTouch", "actionIndex = " + ev.getActionIndex());
        for (int i = 0; i < ev.getPointerCount(); i ++) {
            Log.e("onTouch", "[" + i +"] = {" + ev.getX(i) + ", " + ev.getY(i) + "}, id = " + ev.getPointerId(i));
        }
        Log.e("onTouch", "---------------------------------------------------------------------------");
        //*/

        switch (ev.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                int pointIndex = ev.getActionIndex();
                mRender.setTouchPosition(ev.getPointerId(pointIndex), ev.getX(pointIndex), ev.getY(pointIndex), ev.getPressure(pointIndex), ev.getPointerId(pointIndex));
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                for (int i = 0; i < ev.getPointerCount(); i ++) {
                    mRender.setTouchPosition(ev.getPointerId(i), ev.getX(i), ev.getY(i), ev.getPressure(i), ev.getPointerId(i));
                }
                break;
            }
            default: {
                break;
            }
        }

        return true;
    }

    @SuppressWarnings("unused")
    private static String getMotionActionString(int action) {
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                return "Down";
            case MotionEvent.ACTION_UP:
                return "Up";
            case MotionEvent.ACTION_MOVE:
                return "Move";
            case MotionEvent.ACTION_CANCEL:
                return "Cancel";
            case MotionEvent.ACTION_OUTSIDE:
                return "Outside";
            case MotionEvent.ACTION_POINTER_DOWN:
                return "Pointer Down";
            case MotionEvent.ACTION_POINTER_UP:
                return "Pointer Up";
            case MotionEvent.ACTION_HOVER_MOVE:
                return "Hover Move";
            case MotionEvent.ACTION_SCROLL:
                return "Scroll";
            case MotionEvent.ACTION_HOVER_ENTER:
                return "Hover Enter";
            case MotionEvent.ACTION_HOVER_EXIT:
                return "Hover Exit";
        }

        return ("Unknown: " + action);
    }

}